package basicTest;

import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class CRUDItemsTest {
    protected String user = "yawar@gmail.com";
    protected String pwd = "yawar123";

    @Test
    public void testCreateItem() {
        final String content = "Create item test";
        JSONObject body= new JSONObject();
        body.put("Content", content);

        given()
                .auth()
                .preemptive()
                .basic(user,pwd)
                .body(body.toString())
                .log()
                .all()
                .when()
                .post("https://todo.ly/api/items.json")
                .then()
                .statusCode(200)
                .body("Content", equalTo(content))
                .log()
                .all();
    }

    @Test
    public void testReadItem() {
        final String content = "Read item test";
        JSONObject body= new JSONObject();
        body.put("Content", content);

        Response response=given()
                .auth()
                .preemptive()
                .basic(user,pwd)
                .body(body.toString())
                .log()
                .all()
                .when()
                .post("https://todo.ly/api/items.json");
        response.then()
                .statusCode(200)
                .body("Content", equalTo(content))
                .log()
                .all();

        int id = response.then().extract().path("Id");

        response = given()
                .auth()
                .preemptive()
                .basic(user,pwd)
                .log()
                .all()
                .when()
                .get("https://todo.ly/api/items/" + id + ".json");

        response.then()
                .statusCode(200)
                .body("Content", equalTo(content))
                .log()
                .all();
    }

    @Test
    public void testUpdateItem() {
        final String content = "Update item test";
        JSONObject body= new JSONObject();
        body.put("Content", content);

        Response response=given()
                .auth()
                .preemptive()
                .basic(user,pwd)
                .body(body.toString())
                .log()
                .all()
                .when()
                .post("https://todo.ly/api/items.json");
        response.then()
                .statusCode(200)
                .body("Content", equalTo(content))
                .log()
                .all();

        int id = response.then().extract().path("Id");

        final String updatedContent = "Updated";
        body.put("Content", updatedContent);
        body.put("Checked", true);

        response = given()
                .auth()
                .preemptive()
                .basic(user,pwd)
                .body(body.toString())
                .log()
                .all()
                .when()
                .put("https://todo.ly/api/items/" + id + ".json");
        response.then()
                .statusCode(200)
                .body("Content", equalTo(updatedContent))
                .body("Checked",equalTo(true))
                .log()
                .all();
    }

    @Test
    public void testDeleteItem() {
        final String content = "Delete item test";
        JSONObject body= new JSONObject();
        body.put("Content", content);

        Response response=given()
                .auth()
                .preemptive()
                .basic(user,pwd)
                .body(body.toString())
                .log()
                .all()
                .when()
                .post("https://todo.ly/api/items.json");
        response.then()
                .statusCode(200)
                .body("Content", equalTo(content))
                .log()
                .all();

        int id = response.then().extract().path("Id");

        response=given()
                .auth()
                .preemptive()
                .basic(user,pwd)
                .log()
                .all()
                .when()
                .delete("https://todo.ly/api/items/" + id + ".json");
        response.then()
                .statusCode(200)
                .body("Content", equalTo(content))
                .body("Deleted",equalTo(true))
                .log()
                .all();
    }
}
